# Task Based UI:
1. Focus each page on a single task
2. Make your tasks be like commands
3. Be mindful of your model:
	a. Your model follows your business process
	b. Prevents CRUD-based UI
4. Check your links:
	a. Minimize links to other pages not part of the main task
	b. Keep track of links needed to complete each task
	c. Make the effect of each action clear
5. Follow Established UI Patterns
